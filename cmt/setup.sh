# echo "Setting PIDConfig v1r0 in /build/jenkins-tests/workspace/nightly-builds/checkout/tmp/checkout/DBASE/WG"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=PIDConfig -version=v1r0 -path=/build/jenkins-tests/workspace/nightly-builds/checkout/tmp/checkout/DBASE/WG  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

