# echo "Setting PIDConfig v1r0 in /build/jenkins-tests/workspace/nightly-builds/checkout/tmp/checkout/DBASE/WG"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=PIDConfig -version=v1r0 -path=/build/jenkins-tests/workspace/nightly-builds/checkout/tmp/checkout/DBASE/WG  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

